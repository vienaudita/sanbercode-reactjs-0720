// Soal 1
// gabungkan variabel-variabel tersebut agar menghasilkan output
// saya Senang belajar JAVASCRIPT

var kataPertama = "saya ";
var kataKedua = "senang ";
var kataKetiga = "belajar ";
var kataKeempat = "javascript ";


var senang = kataKedua[0].toLocaleUpperCase() + kataKedua.substring(1, 7);
var upperJavascript = kataKeempat.toUpperCase();
var gabung = kataPertama.concat(senang, kataKetiga, upperJavascript);

console.log(gabung);

// Soal 2
// ubah lah variabel diatas ke dalam integer dan lakukan jumlahkan semua variabel dan tampilkan dalam output

var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";

var numberPertama = parseInt(kataPertama);
var numberKedua = parseInt(kataKedua);
var numberKetiga = parseInt(kataKetiga);
var numberKeempat = parseInt(kataKeempat);

var penjumlahan = numberPertama + numberKedua + numberKetiga + numberKeempat;

console.log(penjumlahan);


// Soal 3

var kalimat = 'wah javascript itu keren sekali';

var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substring(4, 14);
var kataKetiga = kalimat.substring(15, 18);
var kataKeempat = kalimat.substring(19, 25);
var kataKelima = kalimat.substring(25, 32);

console.log('Kata Pertama: ' + kataPertama);
console.log('Kata Kedua: ' + kataKedua);
console.log('Kata Ketiga: ' + kataKetiga);
console.log('Kata Keempat: ' + kataKeempat);
console.log('Kata Kelima: ' + kataKelima);

// Soal 4

var nilai = 35;

if (nilai >= 80) {
    console.log("A");
} else if (nilai >= 70 && nilai < 80) {
    console.log("B");
}
else if (nilai >= 60 && nilai < 70) {
    console.log("C");
}
else if (nilai >= 50 && nilai < 60) {
    console.log("D");
}
else {
    console.log("E");
}


// soal 5

var tanggal = 23;
var bulan = 3;
var tahun = 1996;

switch (bulan) {
    case 1: { console.log("Januari"); break; }
    case 2: { console.log("February"); break; }
    case 3: { console.log("Maret"); break; }
    case 4: { console.log("April"); break; }
    case 5: { console.log("Mei"); break; }
    case 6: { console.log("Juni"); break; }
    case 7: { console.log("Juli"); break; }
    case 8: { console.log("Agustus"); break; }
    case 9: { console.log("September"); break; }
    case 10: { console.log("Oktober"); break; }
    case 11: { console.log("November"); break; }
    case 12: { console.log("Desember"); break; }
    default: { console.log("Masukkan Bulan Lahir anda"); }

}




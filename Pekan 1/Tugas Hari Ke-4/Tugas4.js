// Soal 1
// LOOPING PERTAMA

var deret = 0;

while (deret < 20) {
    deret += 1;
    deret++;
    console.log(deret + " - I love coding");
}

// LOOPING KEDUA

var deret2 = 22;


while (deret2 > 2) {
    deret2 -= 1;
    deret2--;
    console.log(deret2 + " - I will become a frontend developer")
}

// Soal 2

for (var angka = 1; angka <= 20; angka++) {
    if (angka % 2 == 0) {
        console.log(angka + " - Santai");
    }
    else if (angka % 3 == 0 && angka % 2 == 1) {
        console.log(angka + " - I love coding");
    }
    else if (angka % 2 == 1) {
        console.log(angka + " - Berkualitas");
    }

}


// Soal 3

var i = "";

for (var p = 0; p <= 6; p++) {
    for (var q = 0; q <= p; q++) {
    }

    console.log(i += "#");
}


// Soal 4

var kalimat = "saya sangat senang belajar javascript"

var kalimatArr = kalimat.split(" ")

console.log(kalimatArr);

// Soal 5

var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
var buahSort = daftarBuah.sort()

for (var i = 0; i < 5; i++) {
    console.log(buahSort[i])
}


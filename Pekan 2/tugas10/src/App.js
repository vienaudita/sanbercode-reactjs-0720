import React from 'react';
import logo from './logo.svg';
import './App.css';


function App() {
  return (
    <div style={{ border: '1px solid black', marginTop: '10px', marginRight: '10px', marginLeft: '10px' }}>
      <h1 style={{ textAlign: 'center' }}>Form Pembelian Buah</h1>
      <table>
        <tr>
          <td><span style={{ fontWeight: 'bold', marginLeft: '10px' }}>Nama Pelanggan :</span></td>
          <td><input type="text"></input></td>
        </tr>

        <tr>
          <td><span style={{ marginTop: '25px', fontWeight: 'bold', marginLeft: '10px' }}>Daftar Item :</span></td>
          <input type="checkbox" style={{ marginTop: '20px' }} name="semangka" id="semangka"></input> <label for="#semangka">Semangka</label>
        </tr>

        <tr>
          <td></td>
          <td><input type="checkbox" name="jeruk" id="jeruk"></input> <label for="#jeruk">Jeruk</label></td>
        </tr>

        <tr>
          <td></td>
          <td><input type="checkbox" name="nanas" id="nanas"></input> <label for="#jeruk">Nanas</label></td>
        </tr>

        <tr>
          <td></td>
          <td><input type="checkbox" name="salak" id="salak"></input> <label for="#salak">Salak</label></td>
        </tr>

        <tr>
          <td></td>
          <td><input type="checkbox" name="anggur" id="anggur"></input> <label for="#anggur">Anggur</label></td>
        </tr>


      </table>


      <button type="submit" style={{ borderRadius: '10px', marginTop: '20px', marginBottom: '10px', marginLeft: '10px' }}>Kirim</button>
    </div>
  );
}

export default App;

// Soal 1

// ES 5

// const newFunction = function literal(firstName, lastName) {
//     return {
//         firstName: firstName,
//         lastName: lastName,
//         fullName: function () {
//             console.log(firstName + " " + lastName)
//             return
//         }
//     }
// }

// //Driver Code 
// newFunction("William", "Imoh").fullName()

// jawaban soal 1

function nama(namaDepan, namaBelakang) {
    const namaLengkap = { namaDepan, namaBelakang }
    return namaDepan + " " + namaBelakang;
}


nama();
console.log(nama("vien", "audita"))


// Soal 2

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}



// jawaban soal 2

const { firstName, lastName, destination, occupation, spell } = newObject

console.log(firstName, lastName, destination, occupation)


// Soal 3

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]


// jawaban soal 3
const combined = [...west, ...east]
console.log(combined)


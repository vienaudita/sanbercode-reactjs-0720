// Soal 1

// Luas Lingkaran

let luasLingkaran = (r) => {
    const pi = 22.7
    return pi * r * r
}

console.log(luasLingkaran(5));

// keliling Lingkaran

let kelilingLingkaran = (r) => {
    const pii = 22.7;
    return 2 * pii * r
}


console.log(kelilingLingkaran(5))


// Soal 2

let kalimat = "";

fungsi = () => {

    const saya = "saya";
    const adalah = "adalah";
    const seorang = "seorang";
    const frontend = "frontend";
    const developer = "developer";

    const kata = `${saya} ${adalah} ${seorang} ${frontend} ${developer}`

    return kalimat += kata

}


fungsi();
console.log(kalimat)


// Soal 3

class book {
    constructor(name, totalpage, price) {
        this.name = name
        this.totalpage = totalpage
        this.price = price
    }

    present() {
        return `Nama Buku :${this.name}, Total Page :${this.totalpage}, Price :${this.price}`
    }
}

myBook = new book("Finance", "350 halaman", "Rp.350.000");
console.log(myBook.present());




class Komik extends book {
    constructor(name, totalpage, price, isColorful) {
        super(name, totalpage, price);
        this.isColorful = true;

    }

    show() {
        return this.present() + `, Is Colorful : ${this.isColorful}`;
    }
}

myKomik = new Komik("Komik", "35 Halaman", "Rp.180.000");

console.log(myKomik.show())